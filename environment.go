// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"unicode/utf16"
	"unsafe"
)

// An Environment represents a connection to an ODBC driver.
//
// This function calls the SQLAllocHandle to create the environment.
// It also call SQLEnvSetAttr to set the version of ODBC to version 3.
type Environment struct {
	private int
}

// Connect is a helper function that allocates a connection, and then
// attempts to connect to a data source.
func (e *Environment) Connect(server, username, authentication string) (*Connection, error) {
	dbc, err := e.NewConnection()
	if err != nil {
		return nil, err
	}
	err = dbc.Connect(server, username, authentication)
	if err != nil {
		dbc.Free()
		return nil, err
	}
	return dbc, nil
}

// DriverConnect is a helper function that allocates a connection, and then
// attempts to connect to a data source using a DSN.
func (e *Environment) DriverConnect(dsn string) (*Connection, error) {
	dbc, err := e.NewConnection()
	if err != nil {
		return nil, err
	}
	err = dbc.DriverConnect(dsn)
	if err != nil {
		dbc.Free()
		return nil, err
	}
	return dbc, nil
}

// ConnectionPooling returns a value indicating how the environment handles
// connection pooling.
//
// This member wraps behaviour of calling SQLGetEnvAttr.
func (e *Environment) ConnectionPooling() (ConnectionPooling, error) {
	var ret int32
	err := e.getAttr(201 /*SQL_ATTR_CONNECTION_POOLING*/, unsafe.Pointer(&ret), 0, nil)
	return ConnectionPooling(ret), err
}

// ConnectionPoolingMatchs returns a value indicating how the environment matches
// different connection handles to determine if they can be interchanged.
//
// This member wraps the C API function SQLGetEnvAttr.
func (e *Environment) ConnectionPoolingMatch() (ConnectionPoolingMatch, error) {
	var ret int32
	err := e.getAttr(202 /*SQL_ATTR_CP_MATCH*/, unsafe.Pointer(&ret), 0, nil)
	return ConnectionPoolingMatch(ret), err
}

// Drivers returns a description of the data sources installed on the system.
//
// This function wraps the C API function SQLDataSources.
func (e *Environment) DataSources() ([]DataSource, error) {
	direction := FETCH_FIRST

	var driver [64]uint16
	var driver_ret int16
	var attr [256]uint16
	var attr_ret int16
	var drivers []DataSource

	ret := dataSources(e, direction,
		&driver[0], int16(len(driver)), &driver_ret,
		&attr[0], int16(len(attr)), &attr_ret)
	if ret == 100 /*C.SQL_NO_DATA*/ {
		return nil, nil
	}
	direction = FETCH_NEXT

	for success(ret) {
		tmp := DataSource{
			string(utf16.Decode(driver[0:int(driver_ret)])),
			string(utf16.Decode(attr[0:int(attr_ret)]))}
		drivers = append(drivers, tmp)

		ret := dataSources(e, direction,
			&driver[0], int16(len(driver)), &driver_ret,
			&attr[0], int16(len(attr)), &attr_ret)
		if ret == 100 /*C.SQL_NO_DATA*/ {
			return drivers, nil
		}
	}

	return nil, e.getError()
}

// Drivers returns a description of the drivers installed on the system.
//
// This function wraps the C API function SQLDrivers.
func (e *Environment) Drivers() ([]Driver, error) {
	direction := FETCH_FIRST

	var driver [256]uint16
	var driver_ret int16
	var attr [256]uint16
	var attr_ret int16
	var results []Driver

	ret := drivers(e, direction,
		&driver[0], int16(len(driver)), &driver_ret,
		&attr[0], int16(len(attr)), &attr_ret)
	if ret == 100 /*C.SQL_NO_DATA*/ {
		return nil, nil
	}
	direction = FETCH_NEXT

	for success(ret) {
		tmp := Driver{
			string(utf16.Decode(driver[0:int(driver_ret)])),
			string(utf16.Decode(attr[0:int(attr_ret)]))}
		results = append(results, tmp)

		ret := drivers(e, direction,
			&driver[0], int16(len(driver)), &driver_ret,
			&attr[0], int16(len(attr)), &attr_ret)
		if ret == 100 /*C.SQL_NO_DATA*/ {
			return results, nil
		}
	}

	return nil, e.getError()
}

// EndTransaction either performs either a commit or a rollback of the current transaction.
//
// This function wraps the C API function SQLEndTran.
func (e *Environment) EndTransaction(completion CompletionType) error {
	ret := endTran(HANDLE_ENV, unsafe.Pointer(e), completion)
	if !success(ret) {
		return e.getError()
	}
	return nil
}

// Free releases all resources associated with the environment.
//
// This function wraps the C API function SQLFreeHandle.
func (e *Environment) Free() error {
	ret := freeHandle(HANDLE_ENV, unsafe.Pointer(e))
	if !success(ret) {
		return e.getError()
	}
	return nil
}

// SetConnectionPooling modifies how the environment handles connection pooling.
//
// This member wraps behaviour of calling SQLSetEnvAttr.
func (e *Environment) SetConnectionPooling(value ConnectionPooling) error {
	return e.setAttr(201 /*SQL_ATTR_CONNECTION_POOLING*/, uintptr(value), 0)
}

// SetConnectionPoolingMatch modifies how the environment matches
// different connection handles to determine if they can be interchanged
//
// This member wraps behaviour of calling SQLSetEnvAttr.
func (e *Environment) SetConnectionPoolingMatch(value ConnectionPoolingMatch) error {
	return e.setAttr(202 /*SQL_ATTR_CP_MATCH*/, uintptr(value), 0)
}
