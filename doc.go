// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package odbc3-go provides a thin wrapper around the ODBC API.
//
// The acronym ODBC refers to Open Database Connectivity, and the API provides a
// abstract and low-level interface to different types of database servers.  Drivers
// exist for most major databases types, which means that the API can provide
// an agnostic interface to most databases.
//
// This package provide a thin wrapper around the ODBC API.  This means
// that most functions exist with nearly the same parameters and return values
// as in the underlying C API.  Therefore, documentation and tutorials for ODBC
// should also be useful in understanding this package.  However, some modifications
// in the API are present.  These modifications exist to ensure type-safety, whereas
// the C API frequently requires unsafe casts.
//
// On linux and other unix-like systems, this package uses cgo.  To build this
// package, you will need to have the necessary development files (i.e. headers)
// installed on your system.  You will also need a compiler.
//
// On windows, this package uses the syscall interface.  No development files
// are required.
package odbc3
