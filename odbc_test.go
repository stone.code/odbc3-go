// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"os"
	"reflect"
	"runtime"
	"testing"
)

const (
	FILENAME = "./mysqlite3.db"
)

var (
	driver string
	DSN    string
)

func init() {
	os.Remove(FILENAME)
	if runtime.GOOS == "windows" {
		driver = "SQLite3 ODBC Driver"
	} else {
		driver = "SQLite3"
	}

	DSN = "Driver=" + driver + ";Database=" + FILENAME + ";"
}

func TestAllocEnvironment(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	if env == nil {
		t.Fatalf("NewEnvironement failed to return a pointer without reporting an error.")
	}

	err = env.Free()
	if err != nil {
		t.Fatalf("Could not free environment (%s)", err)
	}
}

func TestEnvironmentConnectionPooling(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	if env == nil {
		t.Fatalf("NewEnvironement failed to return a pointer without reporting an error.")
	}
	defer env.Free()

	cp, err := env.ConnectionPooling()
	if err != nil {
		t.Fatalf("Could not get connection pooling (%s)", err)
	}
	t.Logf("Connection pooling is %v", cp)

	cpm, err := env.ConnectionPoolingMatch()
	if err != nil {
		t.Fatalf("Could not get connection pooling match (%s)", err)
	}
	t.Logf("Connection pooling match is %v", cpm)
}

func TestDataSources(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	sources, err := env.DataSources()
	if err != nil {
		t.Fatalf("Could not get list of drivers (%s)", err)
	}

	for i := 0; i < len(sources); i++ {
		t.Logf("Source %d: %s", i, sources[i].Server)
	}
}

func TestDrivers(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	drivers, err := env.Drivers()
	if err != nil {
		t.Fatalf("Could not get list of drivers (%s)", err)
	}

	for i := 0; i < len(drivers); i++ {
		t.Logf("Driver %d: %s", i, drivers[i].Name)
	}
}

func TestAllocConnection(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.NewConnection()
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}

	err = dbc.Free()
	if err != nil {
		t.Fatalf("Could not free connection (%s)", err)
	}
}

func TestDriverConnect(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not connect (%s)", err)
	}
	defer dbc.Free()
}

func TestAllocStatement(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.NewStatement()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}

	err = stmt.Free()
	if err != nil {
		t.Fatalf("Could not free statement (%s)", err)
	}
}

func do_sql(dbc *Connection, sql string) {
	stmt, err := dbc.ExecDirect(sql)
	if err != nil {
		panic(err)
	}
	stmt.Free()
}

func TestExecDirect(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.ExecDirect("CREATE TABLE atable (c1 CHAR(128), c2 INTEGER, c3 REAL, c4 BIT, c5 TIMESTAMP);")
	if err != nil {
		t.Fatalf("Could not execute sql (%s)", err)
	}

	err = stmt.Free()
	if err != nil {
		t.Fatalf("Could not free statement (%s)", err)
	}

	// Create data for following tests
	do_sql(dbc, "INSERT INTO atable VALUES('Hello', 4, 4.5, 0, 0);")
	do_sql(dbc, "INSERT INTO atable VALUES('world', 5, 5.6, 1, '01/01/1998');")
	do_sql(dbc, "INSERT INTO atable VALUES('cruel', 6, 7.8, 0, '01/01/1998 23:22:21');")
	do_sql(dbc, "INSERT INTO atable VALUES('beautiful', 7, 9.7, 1, '01/01/1998 23:22:23');")
}

func TestExecDirect2(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.ExecDirect("SELECT * FROM atable;")
	if err != nil {
		t.Fatalf("Could not execute sql (%s)", err)
	}
	defer stmt.Free()

	i := 0
	for {
		ok, err := stmt.Fetch()
		if err != nil {
			t.Fatalf("Error while fetching rows (%s)", err)
		}
		if !ok {
			break
		}
		i++
	}
	if i != 4 {
		t.Errorf("Did not find the correct number of rows (%d)", i)
	}
}

func TestExecDirect3(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.ExecDirect("SELECT * FROM atable;")
	if err != nil {
		t.Fatalf("Could not execute sql (%s)", err)
	}
	defer stmt.Free()

	ok, err := stmt.Fetch()
	if err != nil {
		t.Fatalf("Error while fetching rows (%s)", err)
	}
	if !ok {
		t.Errorf("No rows in result set?")
	}

	numcols, err := stmt.NumResultCols()
	if err != nil {
		t.Fatalf("Could not execute sql (%s)", err)
	}
	for i := 1; i <= numcols; i++ {
		str, err := stmt.ColAttributeString(i, DESC_NAME)
		t.Logf("Col %d : DESC_NAME : %v %v", i, str, err)
		num, err := stmt.ColAttributeNumeric(i, DESC_TYPE)
		t.Logf("Col %d : DESC_TYPE : %v %v", i, num, err)
		num, err = stmt.ColAttributeNumeric(i, DESC_NULLABLE)
		t.Logf("Col %d : DESC_NULLABLE : %v %v", i, num, err)
	}
}

func TestExecute(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.Prepare("SELECT * FROM atable;")
	if err != nil {
		t.Fatalf("Could not execute sql (%s)", err)
	}
	defer stmt.Free()

	numparam, err := stmt.NumParams()
	if err != nil {
		t.Errorf("Could not determine the number of parameters (%s)", err)
	}
	if numparam != 0 {
		t.Errorf("Incorrect number of parameters reported (%d)", numparam)
	}

	err = stmt.Execute()
	if err != nil {
		t.Fatalf("Could not execute sql (%s)", err)
	}

	for {
		ok, err := stmt.Fetch()
		if err != nil {
			t.Fatalf("Error while fetching rows (%s)", err)
		}
		if !ok {
			break
		}
	}
}

type QueryTest struct {
	sql   string
	value interface{}
}

var (
	query_tests = []QueryTest{
		{"SELECT c1 FROM atable WHERE c2=4;", "Hello"},
		{"SELECT c2 FROM atable WHERE c3=4.5;", int32(4)},
		{"SELECT c3 FROM atable WHERE c2=6;", float64(7.8)},
		{"SELECT c4 FROM atable WHERE c1='world';", true},
		{"SELECT c4 FROM atable WHERE c1='beautiful';", true},
		{"SELECT c4 FROM atable WHERE c2=7;", true},
		{"SELECT c5 FROM atable WHERE c2=7;", Timestamp{1998, 01, 01, 23, 22, 23, 0}}}
)

func TestGetDataFromDatabase(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	for _, v := range query_tests {
		t.Logf("Testing : %s", v.sql)
		stmt, err := dbc.ExecDirect(v.sql)
		if err != nil {
			t.Errorf("Could not execute sql (%s)", err)
		} else {
			defer stmt.Free()

			ok, err := stmt.Fetch()
			if err != nil {
				t.Errorf("Did not get any data (%s)", err)
			}
			if !ok {
				t.Errorf("Did not get any data (no data)")
			} else {
				numdata, err := stmt.NumResultCols()
				if numdata != 1 || err != nil {
					t.Errorf("Did not get the correct number of columns")
				}

				value, err := stmt.GetData(1)
				if err != nil {
					t.Errorf("Could not get data (%s)", err)
				}

				if !reflect.DeepEqual(v.value, value) {
					t.Errorf("Did not receive correct value (%v) (%v)", value, v.value)
				}
			}
		}
	}
}

func TestGetDataBytes(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.ExecDirect("SELECT c1,c1 FROM atable;")
	if err != nil {
		t.Fatalf("Could not execute sql statement (%s)", err)
	}
	defer stmt.Free()

	for {
		ok, err := stmt.Fetch()
		if err != nil {
			t.Fatalf("Could not fetch data (%s)", err)
		}
		if !ok {
			break
		}

		bytes, err := stmt.GetDataBytes(1)
		if err != nil {
			t.Fatalf("Could not fetch data (%s)", err)
		}
		str, ok, err := stmt.GetDataString(2)
		if err != nil {
			t.Fatalf("Could not fetch data (%s)", err)
		}

		if string(bytes) != str {
			t.Fatalf("Data retreived using GetDataBytes does not match GetDataString.")
		}
	}
}

func TestRowCount(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.ExecDirect("INSERT INTO atable VALUES ('spherical',8,1.1,1,0);")
	if err != nil {
		t.Fatalf("Could not execute the sql statement (%s)", err)
	}
	defer stmt.Free()

	numrows, err := stmt.RowCount()
	if err != nil {
		t.Fatalf("Could not obtain the row count (%s)", err)
	}
	if numrows != 1 {
		t.Errorf("Row count was incorrect (%d)", numrows)
	}
}

func TestRollback(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	ac, err := dbc.AutoCommit()
	if err != nil {
		t.Fatalf("Could not determine state of autocommit (%s)", err)
	}
	if !ac {
		t.Logf("Autocommit was not on, as expected by default")
	}

	err = dbc.SetAutoCommit(false)
	if err != nil {
		t.Fatalf("Could not turn off autocommit (%s)", err)
	}

	do_sql(dbc, "INSERT INTO atable VALUES ('aaspherical',8,1.1,1,0);")
	do_sql(dbc, "INSERT INTO atable VALUES ('bbspherical',8,1.1,1,0);")

	err = dbc.EndTransaction(ROLLBACK)
	if err != nil {
		t.Fatalf("Could not rollback transaction (%s)", err)
	}
}

func TestCommit(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	ac, err := dbc.AutoCommit()
	if err != nil {
		t.Fatalf("Could not determine state of autocommit (%s)", err)
	}
	if !ac {
		t.Logf("Autocommit was not on, as expected by default")
	}

	err = dbc.SetAutoCommit(false)
	if err != nil {
		t.Fatalf("Could not turn off autocommit (%s)", err)
	}

	do_sql(dbc, "INSERT INTO atable VALUES ('aaspherical',8,1.1,1,0);")
	do_sql(dbc, "INSERT INTO atable VALUES ('bbspherical',8,1.1,1,0);")

	err = dbc.EndTransaction(COMMIT)
	if err != nil {
		t.Fatalf("Could not commit transaction (%s)", err)
	}
}
