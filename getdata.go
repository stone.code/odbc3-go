// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"unsafe"
)

// GetData retrieves data from a single column.
//
// This function determines the type of the SQL value of the requested column,
// and coerces that value into the nearest Go type.
//
// If the SQL data is NULL, than this function will return a nil interface.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetData(index int) (interface{}, error) {
	ft, err := s.ColAttributeNumeric(index, DESC_TYPE)
	if err != nil {
		return nil, err
	}

	switch Kind(ft) {
	case INTEGER:
		num, ok, err := s.GetDataInt32(index)
		if err != nil || !ok {
			return nil, err
		}
		return num, nil
	case BIGINT:
		num, ok, err := s.GetDataInt64(index)
		if err != nil || !ok {
			return nil, err
		}
		return num, nil
	case FLOAT, REAL, DOUBLE:
		num, ok, err := s.GetDataFloat64(index)
		if err != nil || !ok {
			return nil, err
		}
		return num, nil

	case CHAR, VARCHAR:
		str, ok, err := s.getDataString(index)
		if err != nil || !ok {
			return nil, err
		}
		return str, nil

	case WCHAR, WVARCHAR:
		str, ok, err := s.getDataStringW(index)
		if err != nil || !ok {
			return nil, err
		}
		return str, nil

	case BIT:
		bl, ok, err := s.GetDataBool(index)
		if err != nil || !ok {
			return nil, err
		}
		return bl, nil

	case DATE, TIME, TIMESTAMP, TYPE_DATE, TYPE_TIME, TYPE_TIMESTAMP:
		tm, ok, err := s.GetDataTimestamp(index)
		if err != nil || !ok {
			return nil, err
		}
		return tm, nil

		// TODO:  add support for other types
		// such as blobs ([]byte) and
	}

	return nil, ErrUnsupportedOdbcType
}

// GetDataInt32 retrieves data from a single column, and attempts to coerce that value into an int32.
//
// If the SQL data is null, this function will return 0.  Additionally, the second return value will be true.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetDataInt32(index int) (value int32, ok bool, err error) {
	var fl int32
	ret := getData(s, uint16(index), int16(C_SLONG),
		unsafe.Pointer(&value), 0, &fl)
	if !success(ret) {
		return 0, false, s.getError()
	}
	return value, fl >= 0, nil
}

// GetDataInt64 retrieves data from a single column, and attempts to coerce that value into an int64.
//
// If the SQL data is null, this function will return 0.  Additionally, the second return value will be true.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetDataInt64(index int) (value int64, ok bool, err error) {
	var fl int32
	ret := getData(s, uint16(index), int16(C_SBIGINT),
		unsafe.Pointer(&value), 0, &fl)
	if !success(ret) {
		return 0, false, s.getError()
	}
	return value, fl >= 0, nil
}

// GetDataFloat64 retrieves data from a single column, and attempts to coerce that value into an float64.
//
// If the SQL data is null, this function will return 0.  Additionally, the second return value will be true.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetDataFloat64(index int) (value float64, isnull bool, err error) {
	var fl int32
	ret := getData(s, uint16(index), int16(C_DOUBLE),
		unsafe.Pointer(&value), 0, &fl)
	if !success(ret) {
		return 0, false, s.getError()
	}
	return value, fl >= 0, nil
}

// GetDataBool retrieves data from a single column, and attempts to coerce that value into an bool.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetDataBool(index int) (value bool, ok bool, err error) {
	var buffer byte
	var fl int32
	ret := getData(s, uint16(index), int16(C_BIT),
		unsafe.Pointer(&buffer), 1, &fl)
	if !success(ret) {
		return false, false, s.getError()
	}
	return buffer != 0, fl >= 0, nil
}

// GetDataString retrieves data from a single column, and attempts to coerce that value into an string.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetDataString(index int) (value string, ok bool, err error) {
	field_type, err := s.ColAttributeNumeric(index, DESC_TYPE)
	if err != nil {
		return "", false, err
	}

	if Kind(field_type) == WCHAR || Kind(field_type) == WVARCHAR {
		return s.getDataStringW(index)
	}

	return s.getDataString(index)
}

func (s *Statement) getDataString(index int) (value string, ok bool, err error) {
	parts := make([]byte, 0)

	for {
		chunkSize := int32(2048)
		chunk := make([]byte, chunkSize)
		var fl int32

		ret := getData(s, uint16(index), int16(C_CHAR), unsafe.Pointer(&chunk[0]), chunkSize, &fl)
		if ret == 100 /*C.SQL_NO_DATA*/ {
			break
		}
		if !success(ret) {
			return "", false, s.getError()
		}
		if fl < 0 {
			return "", false, nil
		}
		parts = append(parts, chunk[0:fl]...)
	}

	return string(parts), true, nil
}

// GetDataTimestamp retrieves data from a single column, and attempts to coerce that value into an time.Time.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetDataTimestamp(index int) (value Timestamp, ok bool, err error) {
	var fl int32
	ret := getData(s, uint16(index), int16(C_TIMESTAMP),
		unsafe.Pointer(&value), 0, &fl)
	if !success(ret) {
		return Timestamp{}, false, s.getError()
	}
	if fl < 0 {
		return Timestamp{}, false, nil
	}
	return value, true, nil
}

// GetDataBytes retrieves data from a single column, and attempts to coerce that value into a slice of bytes.
//
// If the SQL data is NULL, than the byte slice will be nil.
//
// This function wraps the C API function SQLGetData.
func (s *Statement) GetDataBytes(index int) ([]byte, error) {
	parts := make([]byte, 0)

	for {
		chunkSize := int32(2048)
		chunk := make([]byte, chunkSize)
		var fl int32

		ret := getData(s, uint16(index), int16(C_CHAR), unsafe.Pointer(&chunk[0]), chunkSize, &fl)
		if ret == 100 /*C.SQL_NO_DATA*/ {
			break
		}
		if !success(ret) {
			return nil, s.getError()
		}
		if fl < 0 {
			return nil, nil
		}
		parts = append(parts, chunk[0:fl]...)
	}

	return parts, nil
}
