// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build windows

package odbc3

// Return values
const (
	sql_null_data         = sqlreturn(-1)
	sql_data_at_exec      = sqlreturn(-2)
	sql_success           = sqlreturn(0)
	sql_success_with_info = sqlreturn(1)
	sql_error             = sqlreturn(-1)
	sql_invalid_handle    = sqlreturn(-2)
	sql_no_data           = sqlreturn(100)
)
