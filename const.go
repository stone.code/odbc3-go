// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

// A HandleType enumerates the different type of handle used by the ODBC API.
// Most users should not need to use this type.
type HandleType int16

const (
	HANDLE_ENV  = HandleType(1)
	HANDLE_DBC  = HandleType(2)
	HANDLE_STMT = HandleType(3)
	HANDLE_DESC = HandleType(4)
)

// A Kind represents the specific data type of a parameter or column used in
// internally by an SQL query.  Most users of this package should not need to
// use these parameters.
type Kind int16

const (
	UNKNOWN_TYPE   = Kind(0)
	CHAR           = Kind(1)
	NUMERIC        = Kind(2)
	DECIMAL        = Kind(3)
	INTEGER        = Kind(4)
	SMALLINT       = Kind(5)
	FLOAT          = Kind(6)
	REAL           = Kind(7)
	DOUBLE         = Kind(8)
	DATETIME       = Kind(9)
	DATE           = Kind(9)
	TIME           = Kind(10)
	TIMESTAMP      = Kind(11)
	VARCHAR        = Kind(12)
	TYPE_DATE      = Kind(91)
	TYPE_TIME      = Kind(92)
	TYPE_TIMESTAMP = Kind(93)
	BINARY         = Kind(-2)
	VARBINARY      = Kind(-3)
	BIGINT         = Kind(-5)
	TINYINT        = Kind(-6)
	BIT            = Kind(-7)
	WCHAR          = Kind(-8)
	WVARCHAR       = Kind(-9)
)

// A CKind represents the specific data type of a parameter or column used externally (i.e. in the calling code).
// Most users of this package should not need to use these parameters.
type CKind int16

const (
	C_CHAR           = CKind(CHAR)
	C_DOUBLE         = CKind(DOUBLE)
	C_DATE           = CKind(DATE)
	C_TIME           = CKind(TIME)
	C_TIMESTAMP      = CKind(TIMESTAMP)
	C_BIT            = CKind(BIT)
	C_WCHAR          = CKind(WCHAR)
	C_TYPE_DATE      = CKind(TYPE_DATE)
	C_TYPE_TIME      = CKind(TYPE_TIME)
	C_TYPE_TIMESTAMP = CKind(TYPE_TIMESTAMP)
	C_BINARY         = CKind(BINARY)
	C_DEFAULT        = CKind(99)
	C_SLONG          = CKind(-16)
	C_SBIGINT        = CKind(-25)
)

// A FieldIdentifier is used to select various attributes that can be reported
// when using the member function ColAttributeNumeric or ColAttributeString.
type FieldIdentifier uint16

const (
	DESC_AUTO_UNIQUE_VALUE      = FieldIdentifier(11)
	DESC_BASE_COLUMN_NAME       = FieldIdentifier(22)
	DESC_BASE_TABLE_NAME        = FieldIdentifier(23)
	DESC_CASE_SENSITIVE         = FieldIdentifier(12)
	DESC_COUNT                  = FieldIdentifier(1001)
	DESC_TYPE                   = FieldIdentifier(1002)
	DESC_LENGTH                 = FieldIdentifier(1003)
	DESC_OCTET_LENGTH_PTR       = FieldIdentifier(1004)
	DESC_PRECISION              = FieldIdentifier(1005)
	DESC_SCALE                  = FieldIdentifier(1006)
	DESC_DATETIME_INTERVAL_CODE = FieldIdentifier(1007)
	DESC_NULLABLE               = FieldIdentifier(1008)
	DESC_INDICATOR_PTR          = FieldIdentifier(1009)
	DESC_DATA_PTR               = FieldIdentifier(1010)
	DESC_NAME                   = FieldIdentifier(1011)
	DESC_UNNAMED                = FieldIdentifier(1012)
	DESC_OCTET_LENGTH           = FieldIdentifier(1013)
	DESC_ALLOC_TYPE             = FieldIdentifier(1099)
)

type InputOutputType int16

const (
	PARAM_TYPE_UNKNOWN = InputOutputType(0)
	PARAM_INPUT        = InputOutputType(1)
	PARAM_INPUT_OUTPUT = InputOutputType(2)
	RESULT_COL         = InputOutputType(3)
	PARAM_OUTPUT       = InputOutputType(4)
	RETURN_VALUE       = InputOutputType(5)
)

// A FetchDirection is used to control record seeking in calls to FetchScroll.
type FetchDirection uint16

const (
	FETCH_NEXT     = FetchDirection(1)
	FETCH_FIRST    = FetchDirection(2)
	FETCH_LAST     = FetchDirection(3)
	FETCH_PRIOR    = FetchDirection(4)
	FETCH_ABSOLUTE = FetchDirection(5)
	FETCH_RELATIVE = FetchDirection(6)
)

// A ConnectionPool specifies how an environment handles connection pooling.
type ConnectionPooling uint32

const (
	CP_OFF            = ConnectionPooling(0)
	CP_ONE_PER_DRIVER = ConnectionPooling(1)
	CP_ONE_PER_HENV   = ConnectionPooling(2)
	CP_DEFAULT        = CP_OFF
)

// A ConnectionPoolMatch specifies how strictly an environment matches connections.
type ConnectionPoolingMatch uint32

const (
	CP_STRICT_MATCH  = ConnectionPoolingMatch(0)
	CP_RELAXED_MATCH = ConnectionPoolingMatch(1)
	CP_MATCH_DEFAULT = CP_STRICT_MATCH
)

// A CompletionType specifies how a transaction in terminated.
type CompletionType int16

const (
	COMMIT   = CompletionType(0)
	ROLLBACK = CompletionType(1)
)

// A DriverCompletion specifies how a driver should prompt for missing information in a DSN string
type DriverCompletion uint16

const (
	DRIVER_NOPROMPT          = DriverCompletion(0)
	DRIVER_COMPLETE          = DriverCompletion(1)
	DRIVER_PROMPT            = DriverCompletion(2)
	DRIVER_COMPLETE_REQUIRED = DriverCompletion(3)
)
