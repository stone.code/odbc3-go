// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build windows

package odbc3

type sqlreturn int16
type sqllen int32

//sys	allocHandle(handleType HandleType, inputHandle unsafe.Pointer, outputHandle *unsafe.Pointer ) (ret sqlreturn) = odbc32.SQLAllocHandle
//sys	freeHandle(handleType HandleType, handle unsafe.Pointer) (ret sqlreturn) = odbc32.SQLFreeHandle
//sys	getDiagRec(handleType HandleType, inputHandle unsafe.Pointer, recNumber int16, sqlState *uint16, nativeErrPtr *int32, messageText *uint16, bufferLength int16, textLengthPtr *int16 ) (ret sqlreturn ) = odbc32.SQLGetDiagRecW
//sys	setEnvAttr(handle *Environment, attribute int32, valuePtr unsafe.Pointer, stringLength int32) (ret sqlreturn) = odbc32.SQLSetEnvAttr
//sys	getEnvAttr(handle *Environment, attribute int32, valuePtr unsafe.Pointer, bufferLength int32, stringLengthPtr *int32) (ret sqlreturn) = odbc32.SQLGetEnvAttr
//sys	setConnectAttr(handle *Connection, attribute int32, valuePtr unsafe.Pointer, stringLength int32) (ret sqlreturn) = odbc32.SQLSetConnectAttrW
//sys	getConnectAttr(handle *Connection, attribute int32, valuePtr unsafe.Pointer, bufferLength int32, stringLengthPtr *int32) (ret sqlreturn) = odbc32.SQLGetConnectAttrW
//sys	drivers(handle *Environment, direction FetchDirection, driverDescription *uint16, bufferLength1 int16, descriptionLength *int16, attributes *uint16, bufferLength2 int16, attributesLength *int16 ) (ret sqlreturn) = odbc32.SQLDriversW
//sys	dataSources(handle *Environment, direction FetchDirection, driverDescription *uint16, bufferLength1 int16, descriptionLength *int16, attributes *uint16, bufferLength2 int16, attributesLength *int16 ) (ret sqlreturn) = odbc32.SQLDataSourcesW
//sys	connect(handle *Connection, serverName *uint16, nameLength1 int16, userName *uint16, nameLength2 int16, auth *uint16, nameLength3 int16) (ret sqlreturn) = odbc32.SQLConnectW
//sys	driverConnect(handle *Connection, windowHandle unsafe.Pointer, inConnectionString *uint16, stringLength1 int16, outConnectionString *uint16, bufferLength int16, stringLength2Ptr *int16, driverCompletion DriverCompletion ) (ret sqlreturn) = odbc32.SQLDriverConnectW
//sys	execDirect(handle *Statement, statementText *uint16, textLength int32) (ret sqlreturn) = odbc32.SQLExecDirectW
//sys	endTran(handleType HandleType, handle unsafe.Pointer, completionType CompletionType) (ret sqlreturn) = odbc32.SQLEndTran
//sys	fetch(handle *Statement) (ret sqlreturn) = odbc32.SQLFetch
//sys	fetchScroll(handle *Statement, orientation FetchDirection, offset int32) (ret sqlreturn) = odbc32.SQLFetchScroll
//sys	prepare(handle *Statement, statementText *uint16, textLength int32) (ret sqlreturn) = odbc32.SQLPrepareW
//sys	numParams(handle *Statement, parameterCount *int16) (ret sqlreturn) = odbc32.SQLNumParams
//sys	numResultCols(handle *Statement, parameterCount *int16) (ret sqlreturn) = odbc32.SQLNumResultCols
//sys	execute(handle *Statement) (ret sqlreturn) = odbc32.SQLExecute
//sys	colAttribute(handle *Statement, index uint16, field FieldIdentifier, characterAttr *uint16, bufferLength int16, stringLength *int16, numericAttr *uint32) (ret sqlreturn) = odbc32.SQLColAttributeW
//sys	bindParameter(handle *Statement, index uint16, inputOutputType int16, valueType int16, parameterType int16, columnSize uint32, decimalDigits int16, parameterValue unsafe.Pointer, bufferLength int32, stringLen *int32 ) (ret sqlreturn) = odbc32.SQLBindParameter
//sys	getData(handle *Statement, index uint16, targetType int16, targetValue unsafe.Pointer, bufferLength int32, stringLength *int32 ) (ret sqlreturn) = odbc32.SQLGetData
//sys	cancel(handle *Statement) (ret sqlreturn) = odbc32.SQLCancel
//sys	rowCount(handle *Statement, rowCount *int32) (ret sqlreturn) = odbc32.SQLRowCount
