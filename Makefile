SRC = $(wildcard *.go)

all: $(SRC)
	go install

check: $(SRC)
	go test

windows_api.go: windows_syscall.go
	echo "// +build windows\n" > tmp
	$(GOROOT)/src/pkg/syscall/mksyscall_windows.pl windows_syscall.go > tmp2
	cat tmp tmp2 > windows_api.go
	$(RM) tmp tmp2

