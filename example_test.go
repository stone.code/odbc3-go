// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"fmt"
)

func ExampleNewEnvironment() {
	// Allocate a new environement.
	env, _ := NewEnvironment()
	// Ensure that the environment is released when the function returns.
	defer env.Free()
}

func ExampleEnvironment_DriverConnect() {
	// Allocate a new environement.
	env, _ := NewEnvironment()
	// Ensure that the environment is released when the function returns.
	defer env.Free()

	// Connect to a data source using a known DSN
	dbc, _ := env.DriverConnect(DSN)
	// Ensure that the connection is released
	defer dbc.Free()
}

func ExampleEnvironment_NewConnection() {
	// Allocate a new environement.
	env, _ := NewEnvironment()
	// Ensure that the environment is released when the function returns.
	defer env.Free()

	// Allocate a new connection
	dbc, _ := env.NewConnection()
	// Ensure that the connection is released
	defer dbc.Free()

	// Connect to a datasource using a known DSN
	_ = dbc.DriverConnect(DSN)
}

func ExampleStatement_NumParams() {
	// Allocate a new environement.
	env, _ := NewEnvironment()
	// Ensure that the environment is released when the function returns.
	defer env.Free()

	// Allocate a new connection
	dbc, _ := env.DriverConnect(DSN)
	// Ensure that the connection is released
	defer dbc.Free()

	// Prepare an SQL statement.
	// Note that the question marks indicates a parameter to be bound at a later date.
	stmt, _ := dbc.Prepare("SELECT * FROM atable WHERE c1=?;")
	defer stmt.Free()

	np, _ := stmt.NumParams()
	fmt.Println("Number of parameters is: ", np)

	// Output:
	// Number of parameters is:  1
}

func ExampleStatement_NumResultCols() {
	// Allocate a new environement.
	env, _ := NewEnvironment()
	// Ensure that the environment is released when the function returns.
	defer env.Free()

	// Allocate a new connection
	dbc, _ := env.DriverConnect(DSN)
	// Ensure that the connection is released
	defer dbc.Free()

	// Prepare an SQL statement.
	stmt, _ := dbc.Prepare("SELECT c1, c2 FROM atable;")
	defer stmt.Free()

	nr, _ := stmt.NumResultCols()
	fmt.Println("Number of columns is: ", nr)

	// Output:
	// Number of columns is:  2
}

func ExampleStatement_GetDataInt64() {
	// Allocate a new environment.
	env, _ := NewEnvironment()
	// Ensure that the environment is released when the function returns.
	defer env.Free()
	
	// Allocate a new connection
	dbc, _ := env.DriverConnect(DSN)
	// Ensure that the connection is released.
	defer dbc.Free()
	
	// Prepare and execute an SQL statement.
	stmt, _ := dbc.ExecDirect("SELECT 123456789;")
	defer stmt.Free()

	// Once executed, we can determine how many columns exist in the result.
	nr, _ := stmt.NumResultCols()
	fmt.Println("Number of columns is: ", nr)
	
	// Let's fetch our row of data
	ok, _ := stmt.Fetch()
	if !ok {
		fmt.Println( "Oops.  How did this happen?" )
	}
	
	result, ok, err := stmt.GetDataInt64(1)
	if err!=nil {
		fmt.Println( "There was some problem with fetching or converting the data.", err )
	} else if !ok {
		fmt.Println( "Somehow, our literal number become NULL." )
	} else {
		fmt.Println( "Value is: ", result )
	}

	// Output:
	// Number of columns is:  1
	// Value is:  123456789
}

func ExampleStatement_BindParameterNull() {
	// Allocate a new environment.
	env, _ := NewEnvironment()
	// Ensure that the environment is released when the function returns.
	defer env.Free()
	
	// Allocate a new connection
	dbc, _ := env.DriverConnect(DSN)
	// Ensure that the connection is released.
	defer dbc.Free()
	
	// Prepare an SQL statement.
	// Note that the question marks indicates a parameter to be bound at a later date.
	stmt, _ := dbc.Prepare("SELECT ?;")
	defer stmt.Free()

	// Bind a NULL value to the one open parameter.
	_ = stmt.BindParameterNull(1)
	// Execute the statement
	_ = stmt.Execute()
	// Once executed, we can determine how many columns exist in the result.
	nr, _ := stmt.NumResultCols()
	fmt.Println("Number of columns is: ", nr)
	
	// Let's fetch our row of data
	ok, _ := stmt.Fetch()
	if !ok {
		fmt.Println( "Oops.  How did this happen?" )
	}
	
	result, err := stmt.GetData(1)
	if err!=nil {
		fmt.Println( "There was some problem with fetching or converting the data.", err )
	}
	fmt.Println( "Value is: ", result /* should be a nil interface, which represents NULL */ )

	// Output:
	// Number of columns is:  1
	// Value is:  <nil>
}
