// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build !windows

package odbc3

/*
#cgo LDFLAGS: -lodbc

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
*/
import "C"
import (
	"unsafe"
)

const (
	BUFFER_SIZE     = 10 * 1024
	INFO_BUFFER_LEN = 256
)

func (e *Environment) ptr() C.SQLHENV {
	return C.SQLHENV(unsafe.Pointer(e))
}

func (e *Connection) ptr() C.SQLHDBC {
	return C.SQLHDBC(unsafe.Pointer(e))
}

func (e *Statement) ptr() C.SQLHSTMT {
	return C.SQLHSTMT(unsafe.Pointer(e))
}

func success(ret C.SQLRETURN) bool {
	return int(ret) == C.SQL_SUCCESS || int(ret) == C.SQL_SUCCESS_WITH_INFO
}

func freeHandle(ht HandleType, handle unsafe.Pointer) C.SQLRETURN {
	return C.SQLFreeHandle(C.SQLSMALLINT(ht), C.SQLHANDLE(handle))
}

func newError(ht C.SQLSMALLINT, h C.SQLHANDLE) *Error {
	var sqlState [6]uint16
	var nativeError C.SQLINTEGER
	var textLength C.SQLSMALLINT

	messageText := make([]uint16, C.SQL_MAX_MESSAGE_LENGTH)

	err := &Error{}
	i := 0

	for {
		i++
		ret := C.SQLGetDiagRecW(C.SQLSMALLINT(ht),
			h,
			C.SQLSMALLINT(i),
			(*C.SQLWCHAR)(unsafe.Pointer(&sqlState[0])),
			&nativeError,
			(*C.SQLWCHAR)(unsafe.Pointer(&messageText[0])),
			C.SQL_MAX_MESSAGE_LENGTH,
			&textLength)
		if ret == C.SQL_INVALID_HANDLE || ret == C.SQL_NO_DATA {
			break
		}
		if i == 1 {
			err.SQLState = utf16ToString(sqlState[0:])
			err.NativeError = int(nativeError)
		}
		err.ErrorMessage += utf16ToString(messageText)
	}

	return err
}

func NewEnvironment() (*Environment, error) {
	env := unsafe.Pointer(nil)
	ret := C.SQLAllocHandle(C.SQL_HANDLE_ENV, nil, (*C.SQLHANDLE)(&env))
	if !success(ret) {
		//if env==C.SQL_NULL_HENV {
		return nil, ErrAllocHandleFailed
		//}
	}

	ret = C.SQLSetEnvAttr(C.SQLHENV(env), C.SQL_ATTR_ODBC_VERSION,
		C.SQLPOINTER(unsafe.Pointer(uintptr(C.SQL_OV_ODBC3))), 0)
	if !success(ret) {
		err := newError(C.SQL_HANDLE_ENV, C.SQLHANDLE(env))
		C.SQLFreeHandle(C.SQL_HANDLE_ENV, C.SQLHANDLE(env))
		return nil, err
	}

	return (*Environment)(env), nil
}

func dataSources(handle *Environment, direction FetchDirection, serverName *uint16, bufferLength1 int16, nameLength1Ptr *int16, description *uint16, bufferLength2 int16, nameLength2Ptr *int16) C.SQLRETURN {
	return C.SQLDataSourcesW(handle.ptr(), C.SQLUSMALLINT(direction),
		(*C.SQLWCHAR)(unsafe.Pointer(serverName)), C.SQLSMALLINT(bufferLength1), (*C.SQLSMALLINT)(unsafe.Pointer(nameLength1Ptr)),
		(*C.SQLWCHAR)(unsafe.Pointer(description)), C.SQLSMALLINT(bufferLength2), (*C.SQLSMALLINT)(unsafe.Pointer(nameLength2Ptr)))
}

func drivers(handle *Environment, direction FetchDirection, driverDescription *uint16, bufferLength1 int16, descriptionLengthPtr *int16, driverAttributes *uint16, bufferLength2 int16, attributesLengthPtr *int16) C.SQLRETURN {
	return C.SQLDriversW(handle.ptr(), C.SQLUSMALLINT(direction),
		(*C.SQLWCHAR)(unsafe.Pointer(driverDescription)), C.SQLSMALLINT(bufferLength1), (*C.SQLSMALLINT)(unsafe.Pointer(descriptionLengthPtr)),
		(*C.SQLWCHAR)(unsafe.Pointer(driverAttributes)), C.SQLSMALLINT(bufferLength2), (*C.SQLSMALLINT)(unsafe.Pointer(attributesLengthPtr)))
}

func endTran(ht HandleType, handle unsafe.Pointer, completionType CompletionType) C.SQLRETURN {
	return C.SQLEndTran(C.SQLSMALLINT(ht), C.SQLHANDLE(handle), C.SQLSMALLINT(completionType))
}

func (e *Environment) getAttr(attribute int32, valuePtr unsafe.Pointer, bufferLength int32, stringLengthPtr *int32) error {
	ret := C.SQLGetEnvAttr(e.ptr(), C.SQLINTEGER(attribute), C.SQLPOINTER(valuePtr), C.SQLINTEGER(bufferLength), (*C.SQLINTEGER)(unsafe.Pointer(stringLengthPtr)))
	if !success(ret) {
		return e.getError()
	}
	return nil
}

func (e *Environment) getError() error {
	return newError(C.SQL_HANDLE_ENV, C.SQLHANDLE(e))
}

func (e *Environment) setAttr(attribute int32, valuePtr uintptr, stringLength int32) error {
	ret := C.SQLSetEnvAttr(e.ptr(), C.SQLINTEGER(attribute), C.SQLPOINTER(valuePtr), C.SQLINTEGER(stringLength))
	if !success(ret) {
		return e.getError()
	}
	return nil
}

func (e *Environment) NewConnection() (*Connection, error) {
	dbc := unsafe.Pointer(nil)
	ret := C.SQLAllocHandle(C.SQL_HANDLE_DBC, C.SQLHANDLE(e), (*C.SQLHANDLE)(&dbc))
	if !success(ret) {
		return nil, e.getError()
	}
	return (*Connection)(dbc), nil
}

func (c *Connection) getError() error {
	return newError(C.SQL_HANDLE_DBC, C.SQLHANDLE(c))
}

func connect(handle *Connection, serverName *uint16, nameLength1 int16, userName *uint16, nameLength2 int16, auth *uint16, nameLength3 int16) C.SQLRETURN {
	return C.SQLConnectW(handle.ptr(),
		(*C.SQLWCHAR)(unsafe.Pointer(serverName)), C.SQLSMALLINT(nameLength1),
		(*C.SQLWCHAR)(unsafe.Pointer(userName)), C.SQLSMALLINT(nameLength2),
		(*C.SQLWCHAR)(unsafe.Pointer(auth)), C.SQLSMALLINT(nameLength3))
}

func driverConnect(handle *Connection, windowHandle unsafe.Pointer, inConnectionString *uint16, stringLength1 int16, outConnectionString *uint16, bufferLength int16, stringLength2Ptr *int16, driverCompletion DriverCompletion) C.SQLRETURN {
	return C.SQLDriverConnectW(handle.ptr(), C.SQLHWND(windowHandle),
		(*C.SQLWCHAR)(unsafe.Pointer(inConnectionString)), C.SQLSMALLINT(stringLength1),
		(*C.SQLWCHAR)(unsafe.Pointer(outConnectionString)), C.SQLSMALLINT(bufferLength), (*C.SQLSMALLINT)(unsafe.Pointer(stringLength2Ptr)),
		C.SQLUSMALLINT(driverCompletion))
}

func getConnectAttr(handle *Connection, attribute int32, valuePtr unsafe.Pointer, bufferLength int32, stringLengthPtr *int32) C.SQLRETURN {
	return C.SQLGetConnectAttr(handle.ptr(), C.SQLINTEGER(attribute), C.SQLPOINTER(valuePtr), C.SQLINTEGER(bufferLength), (*C.SQLINTEGER)(unsafe.Pointer(stringLengthPtr)))
}

func (c *Connection) NewStatement() (*Statement, error) {
	stmt := unsafe.Pointer(nil)
	ret := C.SQLAllocHandle(C.SQL_HANDLE_STMT, C.SQLHANDLE(c), (*C.SQLHANDLE)(&stmt))
	if !success(ret) {
		return nil, c.getError()
	}
	return (*Statement)(stmt), nil
}

func setConnectAttr(handle *Connection, attribute int32, valuePtr unsafe.Pointer, stringLength int32) C.SQLRETURN {
	return C.SQLSetConnectAttr(handle.ptr(), C.SQLINTEGER(attribute), C.SQLPOINTER(valuePtr), C.SQLINTEGER(stringLength))
}

func bindParameter(handle *Statement, index uint16, inputOutputType int16, valueType int16, parameterType int16, columnSize uint32, decimalDigits int16, parameterValue unsafe.Pointer, bufferLength int32, stringLen *int32) C.SQLRETURN {
	ret := C.SQLBindParameter(handle.ptr(), C.SQLUSMALLINT(index), C.SQLSMALLINT(inputOutputType),
		C.SQLSMALLINT(valueType), C.SQLSMALLINT(parameterType),
		C.SQLULEN(columnSize), C.SQLSMALLINT(decimalDigits),
		C.SQLPOINTER(parameterValue), C.SQLLEN(bufferLength),
		(*C.SQLLEN)(unsafe.Pointer(stringLen)))
	return ret
}

func cancel(handle *Statement) C.SQLRETURN {
	return C.SQLCancel(handle.ptr())
}

func colAttribute(handle *Statement, index uint16, field FieldIdentifier, characterAttr *uint16, bufferLength int16, stringLength *int16, numericAttribute *uint32) C.SQLRETURN {
	return C.SQLColAttribute(handle.ptr(), C.SQLUSMALLINT(index), C.SQLUSMALLINT(field), nil, 0, nil, (*C.SQLLEN)(unsafe.Pointer(numericAttribute)))
}

func execDirect(handle *Statement, sql *uint16, textLength int32) C.SQLRETURN {
	return C.SQLExecDirectW(handle.ptr(),
		(*C.SQLWCHAR)(unsafe.Pointer(sql)), C.SQLINTEGER(textLength))
}

func execute(handle *Statement) C.SQLRETURN {
	return C.SQLExecute(handle.ptr())
}

func fetch(handle *Statement) C.SQLRETURN {
	return C.SQLFetch(handle.ptr())
}

func fetchScroll(handle *Statement, orientation FetchDirection, offset int32) C.SQLRETURN {
	return C.SQLFetchScroll(handle.ptr(), C.SQLSMALLINT(orientation), C.SQLLEN(offset))
}

func getData(handle *Statement, index uint16, targetType int16, targetValue unsafe.Pointer, bufferLength int32, stringLength *int32) C.SQLRETURN {
	ret := C.SQLGetData(handle.ptr(), C.SQLUSMALLINT(index), C.SQLSMALLINT(targetType),
		C.SQLPOINTER(targetValue), C.SQLLEN(bufferLength),
		(*C.SQLLEN)(unsafe.Pointer(stringLength)))
	return ret
}

func (s *Statement) getError() error {
	return newError(C.SQL_HANDLE_STMT, C.SQLHANDLE(s))
}

func numParams(handle *Statement, cnt *int16) C.SQLRETURN {
	return C.SQLNumParams(handle.ptr(), (*C.SQLSMALLINT)(unsafe.Pointer(cnt)))
}

func numResultCols(handle *Statement, cnt *int16) C.SQLRETURN {
	return C.SQLNumResultCols(handle.ptr(), (*C.SQLSMALLINT)(unsafe.Pointer(cnt)))
}

func prepare(handle *Statement, sql *uint16, textLength int32) C.SQLRETURN {
	return C.SQLPrepareW(handle.ptr(), (*C.SQLWCHAR)(unsafe.Pointer(sql)), C.SQLINTEGER(textLength))
}

func rowCount(handle *Statement, cnt *int32) C.SQLRETURN {
	return C.SQLRowCount(handle.ptr(), (*C.SQLLEN)(unsafe.Pointer(cnt)))
}
