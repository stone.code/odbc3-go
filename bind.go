// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"reflect"
	"unsafe"
)

// BindParameter connects a value in Go to a parameter marker in an SQL statement.
//
// This function bind the actual memory location to the parameter.  Therefore, it
// does not accept, for example, a value of type int64, but will accept a value that
// is a pointer to an int64.  That memory location must live as long as the parameter
// is bound.  Failure to follow this rule will result in invalid memory accesses.
//
// This restriction that the underlying memory must exist also applies to slices
// of bytes and to strings.  However, for those values, the memory backing the
// value is used, so points are not expected.
//
// The following types are accepted for the value:  nil, *int32, *int64, *float64, 
// []byte, string, *odbc3.Date, *odbc3.Time, and *odbc3.Timestamp.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameter(index int, value interface{}) error {
	if value == nil {
		return s.BindParameterNull(index)
	}
	switch v := value.(type) {
	case *int32:
		return s.BindParameterInt32(index, v)
	case *int64:
		return s.BindParameterInt64(index, v)
	case *float64:
		return s.BindParameterFloat64(index, v)
	case []byte:
		return s.BindParameterBytes(index, v)
	case string:
		return s.BindParameterString(index, v)
	case *Date:
		return s.BindParameterDate(index,v)
	case *Time:
		return s.BindParameterTime(index,v)
	case *Timestamp:
		return s.BindParameterTimestamp(index,v)
	}
	return ErrUnsupportedBindType
}

// BindParameterInt32 connects an int32 value in Go to a parameter marker in an SQL statement.
// The value has type INTEGER in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterInt32(index int, value *int32) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_SLONG), int16(INTEGER), 0, 0, unsafe.Pointer(value),
		0, nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterInt64 connects an int64 value in Go to a parameter marker in an SQL statement.
// The value has type BIGINT in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterInt64(index int, value *int64) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_SBIGINT), int16(BIGINT), 0, 0, unsafe.Pointer(value),
		0, nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterFloat64 connects a float64 value in Go to a parameter marker in an SQL statement.
// The value has type DOUBLE in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterFloat64(index int, value *float64) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_DOUBLE), int16(DOUBLE), 0, 0, unsafe.Pointer(value),
		0, nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterBytes connects a slice of bytes to a parameter marker in an SQL statement.
// The value has type CHAR in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterBytes(index int, value []byte) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_CHAR), int16(CHAR), 0, 0, unsafe.Pointer(&value[0]),
		int32(len(value)), nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterString connects a string value in Go to a parameter marker in an SQL statement.
// The value has type CHAR in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterString(index int, value string) error {
	hdr := (*reflect.StringHeader)(unsafe.Pointer(&value))

	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_CHAR), int16(CHAR), 0, 0, unsafe.Pointer(hdr.Data),
		int32(len(value)), nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterDate connects a odbc3.Date value in Go to a parameter marker in an SQL statement.
// The value has type DATE in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterDate(index int, value *Date) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_TYPE_DATE), int16(TYPE_DATE), 0, 0, unsafe.Pointer(value),
		int32(unsafe.Sizeof(*value)), nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterTime connects a odbc3.Time value in Go to a parameter marker in an SQL statement.
// The value has type TIME in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterTime(index int, value *Time) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_TYPE_TIME), int16(TYPE_TIME), 0, 0, unsafe.Pointer(value),
		int32(unsafe.Sizeof(*value)), nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterTimestamp connects a odbc3.Timestamp value in Go to a parameter marker in an SQL statement.
// The value has type TIMESTAMP in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterTimestamp(index int, value *Timestamp) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_TYPE_TIMESTAMP), int16(TYPE_TIMESTAMP), 0, 0, unsafe.Pointer(value),
		int32(unsafe.Sizeof(*value)), nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParamaterNull binds the value NULL to a parameter marker in an SQL statement.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterNull(index int) error {
	strlen := int32(-1) /* SQL_NULL_DATA */
	
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_DEFAULT), int16(WCHAR), 1, 0, nil,
		0, &strlen )
	if !success(ret) {
		return s.getError()
	}
	return nil
}
