// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"unicode/utf16"
	"unsafe"
)

// A Connection represents a connection to a data source.
type Connection struct {
	private int
}

// AutoCommit determines if the connection automatically commits each statement when executed.
//
// This function wraps the C API function SQLGetConnectAttr.
func (c *Connection) AutoCommit() (bool, error) {
	var value int32
	ret := getConnectAttr(c, 102 /*SQL_ATTR_AUTOCOMMIT*/, unsafe.Pointer(&value), -6 /*SQL_IS_INTEGER*/, nil)
	if !success(ret) {
		return false, c.getError()
	}
	return value != 0, nil
}

// Connect creates a connection to a data source.
//
// This function wraps the C API function SQLConnect
func (c *Connection) Connect(server, username, authentication string) error {
	u1 := utf16.Encode([]rune(server))
	u2 := utf16.Encode([]rune(username))
	u3 := utf16.Encode([]rune(authentication))

	ret := connect(c, &u1[0], int16(len(u1)), &u2[0], int16(len(u2)), &u3[0], int16(len(u3)))
	if !success(ret) {
		return c.getError()
	}
	return nil
}

// DriverConnect creates a connection to a data source.
//
// This function wraps the C API function SQLDriverConnect
func (c *Connection) DriverConnect(dsn string) error {
	udsn := utf16.Encode([]rune(dsn))

	ret := driverConnect(c, nil, &udsn[0], int16(len(udsn)), nil, 0, nil, DRIVER_NOPROMPT)
	if !success(ret) {
		return c.getError()
	}
	return nil
}

// EndTransaction either performs either a commit or a rollback of the current transaction.
//
// This function wraps the C API function SQLEndTran.
func (c *Connection) EndTransaction(completion CompletionType) error {
	ret := endTran(HANDLE_DBC, unsafe.Pointer(c), completion)
	if !success(ret) {
		return c.getError()
	}
	return nil
}

// Free releases all resources associated with the environment.
//
// This function wraps the C API function SQLFreeHandle.
func (c *Connection) Free() error {
	ret := freeHandle(HANDLE_DBC, unsafe.Pointer(c))
	if !success(ret) {
		return c.getError()
	}
	return nil
}

// ExecDirect is a helper function that allocates a new statement, and then
// executes the provided SQL statement.  Note that the SQL statement is
// execute directly, which means that there is no opportunity to bind
// parameters.
//
// See the method ExecDirect of the type Statement.
func (c *Connection) ExecDirect(sql string) (*Statement, error) {
	stmt, err := c.NewStatement()
	if err != nil {
		return nil, err
	}
	err = stmt.ExecDirect(sql)
	if err != nil {
		stmt.Free()
		return nil, err
	}
	return stmt, nil
}

// GetAttrInt retrieves a attribute describing the connection if that value can be represented as an int32.
//
// This function wraps the C API function SQLGetConnectAttr.
func (c *Connection) GetAttrInt(field FieldIdentifier) (int32, error) {
	var value int32
	ret := getConnectAttr(c, int32(field), unsafe.Pointer(&value), -6 /*SQL_IS_INTEGER*/, nil)
	if !success(ret) {
		return 0, c.getError()
	}
	return value, nil
}

// GetAttrInt retrieves a attribute describing the connection if that value can be represented as an uint32.
//
// This function wraps the C API function SQLGetConnectAttr.
func (c *Connection) GetAttrUint(field FieldIdentifier) (uint32, error) {
	var value uint32
	ret := getConnectAttr(c, int32(field), unsafe.Pointer(&value), -5 /*SQL_IS_UINTEGER*/, nil)
	if !success(ret) {
		return 0, c.getError()
	}
	return value, nil
}

// Prepare is a helper function that allocates a new statement, and then
// prepares the provided SQL statement.  Use the returned Statement to
// bind parameters and then execute the query.
//
// See the method Prepare of the type Statement.
func (c *Connection) Prepare(sql string) (*Statement, error) {
	stmt, err := c.NewStatement()
	if err != nil {
		return nil, err
	}
	err = stmt.Prepare(sql)
	if err != nil {
		stmt.Free()
		return nil, err
	}
	return stmt, nil
}

// SetAutoCommit determines if the connection automatically commits each statement when executed.
//
// This function wraps the C API function SQLGetConnectAttr.
func (c *Connection) SetAutoCommit(value bool) error {
	var tmp uint32
	if value {
		tmp = 1
	}
	ret := setConnectAttr(c, 102 /*SQL_ATTR_AUTOCOMMIT*/, unsafe.Pointer(uintptr(tmp)), -5 /*SQL_IS_UINTEGER*/)
	if !success(ret) {
		return c.getError()
	}
	return nil
}
