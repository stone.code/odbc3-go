// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

// A DataSource struct contains information about an ODBC data source installed
// on the system.
type DataSource struct {
	// Data source name
	Server string
	// A description of the driver associated with the source
	Description string
}
