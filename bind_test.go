// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"testing"
)

type BindTest struct {
	sql   string
	bind  interface{}
	value string
}

var (
	bind_tests = []BindTest{
		{"SELECT ?;", int32(31), "31" },
		{"SELECT ?;", int64(63), "63" },
		{"SELECT ?;", float64(65.1), "65.1" },
		{"SELECT ?;", "Hello, world!", "Hello, world!" },
		{"SELECT ?;", []byte("Hello, world!"), "Hello, world!" },
		{"SELECT ?;", &Date{2006,01,02}, "2006-01-02" },
		{"SELECT ?;", &Time{03,04,05}, "03:04:05" },
		{"SELECT ?;", &Timestamp{2006,01,02,03,04,05,123456789}, "2006-01-02 03:04:05.123" },
	}
)

func TestBindParameter(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	for _, v := range bind_tests {
		t.Logf("Testing : %s", v.sql)
		stmt, err := dbc.Prepare(v.sql)
		if err != nil {
			t.Fatalf("Could not execute sql (%s)", err)
		}
		defer stmt.Free()

		var i32 int32
		var i64 int64
		var f64 float64
		
		switch bind := v.bind.(type) {
		case int32:
			i32 = bind
			err = stmt.BindParameter(1, &i32)
		case int64:
			i64 = bind
			err = stmt.BindParameter( 1, &i64)
		case float64:
			f64 = bind
			err = stmt.BindParameter( 1, &f64)
		case string:
			err = stmt.BindParameter( 1, bind )
		case []byte:
			err = stmt.BindParameter( 1, bind )
		case *Date:
			err = stmt.BindParameter( 1, bind )
		case *Time:
			err = stmt.BindParameter( 1, bind )
		case *Timestamp:
			err = stmt.BindParameter( 1, bind )
		}
		if err != nil {
			t.Fatalf("Could not bind parameter (%s)", err)
		}

		err = stmt.Execute()
		if err != nil {
			t.Fatalf("Could not execute (%s)", err)
		}

		ok, err := stmt.Fetch()
		if err != nil {
			t.Errorf("Did not get any data (%s)", err)
		}
		if !ok {
			t.Errorf("Did not get any data (no data)")
		} else {
			numdata, err := stmt.NumResultCols()
			if numdata != 1 || err != nil {
				t.Errorf("Did not get the correct number of columns")
			}

			value, _, err := stmt.GetDataString(1)
			if err != nil {
				t.Errorf("Could not get data (%s)", err)
			}

			if v.value!=value {
				t.Errorf("Did not receive correct value (%s) (%s)", value, v.value)
			}
		}
	}
}

func TestBindParameterNull(t *testing.T) {
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	stmt, err := dbc.Prepare("SELECT ?;")
	if err != nil {
		t.Fatalf("Could not execute sql (%s)", err)
	}
	defer stmt.Free()

	err = stmt.BindParameter( 1, nil )
	if err != nil {
		t.Fatalf("Could not bind parameter (%s)", err)
	}

	err = stmt.Execute()
	if err != nil {
		t.Fatalf("Could not execute (%s)", err)
	}

	ok, err := stmt.Fetch()
	if err != nil {
		t.Errorf("Did not get any data (%s)", err)
	}
	if !ok {
		t.Errorf("Did not get any data (no data)")
	} else {
		numdata, err := stmt.NumResultCols()
		if numdata != 1 || err != nil {
			t.Errorf("Did not get the correct number of columns")
		}

		value, ok, err := stmt.GetDataString(1)
		if err != nil {
			t.Errorf("Could not get data (%s)", err)
		}
		if ok {
			t.Errorf("Did not receive correct flag for null")
		}
		if ""!=value {
			t.Errorf("Did not receive correct value (%s) for null", value)
		}
	}
}
