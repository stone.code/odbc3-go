// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"errors"
)

var (
	// This error is returned if a call to NewEnvironment fails.
	ErrAllocHandleFailed = errors.New("Could not allocate new SQL handle.")
	// The data type in the query is not supported.  May be returned by the method GetData.
	ErrUnsupportedOdbcType = errors.New("Unsupported type in database.")
	// The data type cannot be used to bind a parameter.  May be return by the method BindParameter.
	ErrUnsupportedBindType = errors.New("Unsupported type in bind parameter.")
)

// An Error contains information about a ODBC access failure.
//
// This type is used to wrap information returned from SQLGetDiagRec.
type Error struct {
	// A five-character SQLSTATE code
	SQLState string
	// A native error code, which is specific to the data source
	NativeError int
	// A user readable description of the error.
	ErrorMessage string
}

// Error returns a string describing the error.
func (e *Error) Error() string {
	if e != nil {
		return e.SQLState + " " + e.ErrorMessage
	}
	return ""
}

// String returns a string describing the error.
func (e *Error) String() string {
	return e.Error()
}
