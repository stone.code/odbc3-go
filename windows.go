// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build windows

package odbc3

import (
	"syscall"
	"unsafe"
)

func newError(ht HandleType, h unsafe.Pointer) *Error {
	var sqlState [6]uint16
	var nativeError int32
	var textLength int16

	messageText := make([]uint16, 512 /*C.SQL_MAX_MESSAGE_LENGTH*/)

	err := &Error{}
	i := 0

	for {
		i++
		ret := getDiagRec(ht,
			h,
			int16(i),
			&sqlState[0],
			&nativeError,
			&messageText[0],
			int16(len(messageText)),
			&textLength)
		if ret == sql_invalid_handle || ret == sql_no_data {
			break
		}
		if i == 1 {
			err.SQLState = syscall.UTF16ToString(sqlState[0:])
			err.NativeError = int(nativeError)
		}
		err.ErrorMessage += syscall.UTF16ToString(messageText)
	}

	return err
}

func success(ret sqlreturn) bool {
	return ret == sql_success || ret == sql_success_with_info
}

func NewEnvironment() (*Environment, error) {
	env := unsafe.Pointer(nil)
	ret := allocHandle(HANDLE_ENV, nil, &env)
	if !success(ret) {
		//if env==C.SQL_NULL_HENV {
		return nil, ErrAllocHandleFailed
		//}
	}

	ret = setEnvAttr((*Environment)(env), 200, /*C.SQL_ATTR_ODBC_VERSION*/
		unsafe.Pointer(uintptr(3)) /*C.SQL_OV_ODBC3*/, 0)
	if !success(ret) {
		err := newError(HANDLE_ENV, env)
		freeHandle(HANDLE_ENV, env)
		return nil, err
	}

	return (*Environment)(env), nil
}

func (e *Environment) getAttr(attribute int32, valuePtr unsafe.Pointer, bufferLength int32, stringLengthPtr *int32) error {
	ret := getEnvAttr(e, attribute, valuePtr, bufferLength, stringLengthPtr)
	if !success(ret) {
		return e.getError()
	}
	return nil
}

func (e *Environment) getError() error {
	return newError(HANDLE_ENV, unsafe.Pointer(e))
}

func (e *Environment) setAttr(attribute int32, valuePtr uintptr, stringLength int32) error {
	ret := setEnvAttr(e, attribute, unsafe.Pointer(valuePtr), stringLength)
	if !success(ret) {
		return e.getError()
	}
	return nil
}

func (e *Environment) NewConnection() (*Connection, error) {
	dbc := unsafe.Pointer(nil)
	ret := allocHandle(HANDLE_DBC, unsafe.Pointer(e), &dbc)
	if !success(ret) {
		return nil, e.getError()
	}
	return (*Connection)(unsafe.Pointer(dbc)), nil
}

func (c *Connection) getError() error {
	return newError(HANDLE_DBC, unsafe.Pointer(c))
}

func (c *Connection) NewStatement() (*Statement, error) {
	stmt := unsafe.Pointer(nil)
	ret := allocHandle(HANDLE_STMT, unsafe.Pointer(c), &stmt)
	if !success(ret) {
		return nil, c.getError()
	}
	return (*Statement)(stmt), nil
}

func (s *Statement) getError() error {
	return newError(HANDLE_STMT, unsafe.Pointer(s))
}
