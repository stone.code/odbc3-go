// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"reflect"
	"unsafe"
)

// BindParameterBinary connects a slice of bytes to a parameter marker in an SQL statement.
// The value has type BINARY in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterBinary(index int, value []byte) error {
	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_BINARY), int16(BINARY), 0, 0, unsafe.Pointer(&value[0]),
		int32(len(value)), nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// BindParameterBinaryString connects a string value in Go to a parameter marker in an SQL statement.
// The value has type BINARY in the SQL statement.
//
// Please look at the documentation for BindParameter for further instructions.
//
// This function wraps the C API function SQLBindParameter.
func (s *Statement) BindParameterBinaryString(index int, value string) error {
	hdr := (*reflect.StringHeader)(unsafe.Pointer(&value))

	ret := bindParameter(s, uint16(index), int16(PARAM_INPUT),
		int16(C_BINARY), int16(BINARY), 0, 0, unsafe.Pointer(hdr.Data),
		int32(len(value)), nil)
	if !success(ret) {
		return s.getError()
	}
	return nil
}
