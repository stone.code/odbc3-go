// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

// A Driver struct contains information about an ODBC driver installed on the
// system.
type Driver struct {
	// Description of the driver.
	Name string
	// A list of driver attributes separated by semi-colons
	Attr string
}
