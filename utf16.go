// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build !windows

package odbc3

import (
	"unicode/utf16"
)

// UTF16ToString returns the UTF-8 encoding of the UTF-16 sequence s,
// with a terminating NUL removed.
//
// This function is a copy of the version used in the Go distribution on
// Windows.  It is only available on non-windows systems.  Otherwise, please
// use syscall.StringToUTF16.
func utf16ToString(s []uint16) string {
	for i, v := range s {
		if v == 0 {
			s = s[0:i]
			break
		}
	}
	return string(utf16.Decode(s))
}
