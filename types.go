// Copyright 2014 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"time"
)

type Date struct {
	Year  int16
	Month uint16
	Day   uint16
}

func NewDate(value time.Time) Date {
	return Date{
		int16(value.Year()),
		uint16(value.Month()),
		uint16(value.Day())}
}

func (d Date) ToTime(loc *time.Location) time.Time {
	if loc == nil {
		loc = time.UTC
	}
	return time.Date(int(d.Year), time.Month(d.Month), int(d.Day), 0, 0, 0, 0, loc)
}

type Time struct {
	Hour   uint16
	Minute uint16
	Second uint16
}

func NewTime(value time.Time) Time {
	return Time{
		uint16(value.Hour()),
		uint16(value.Minute()),
		uint16(value.Second())}
}

type Timestamp struct {
	Year     int16
	Month    uint16
	Day      uint16
	Hour     uint16
	Minute   uint16
	Second   uint16
	Fraction uint32
}

func NewTimestamp(value time.Time) Timestamp {
	return Timestamp{
		int16(value.Year()),
		uint16(value.Month()),
		uint16(value.Day()),
		uint16(value.Hour()),
		uint16(value.Minute()),
		uint16(value.Second()),
		uint32(value.Nanosecond())}
}

func (d Timestamp) ToTime(loc *time.Location) time.Time {
	if loc == nil {
		loc = time.UTC
	}
	return time.Date(int(d.Year), time.Month(d.Month), int(d.Day), int(d.Hour), int(d.Minute), int(d.Second), int(d.Fraction), loc)
}
