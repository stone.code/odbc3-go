// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"testing"
)

type getDataTest struct {
	sql   string
	pred func(*testing.T,*Statement)
}

func checkInt32(expectedValue int32, expectedOk bool) func (t *testing.T, stmt *Statement) {
	return func (t *testing.T, stmt *Statement) {
		value, ok, err := stmt.GetDataInt32(1)
		if err != nil {
			t.Errorf("Could not get data (%s)", err)
		}
		if expectedOk!=ok {
			t.Errorf("Did not receive correct null flag (%v) (%v)", value, expectedOk)
		}
		if expectedValue!= value {
			t.Errorf("Did not receive correct value (%v) (%v)", value, expectedValue)
		}
	}
}

func checkInt64(expectedValue int64, expectedOk bool) func (t *testing.T, stmt *Statement) {
	return func (t *testing.T, stmt *Statement) {
		value, ok, err := stmt.GetDataInt64(1)
		if err != nil {
			t.Errorf("Could not get data (%s)", err)
		}
		if expectedOk!=ok {
			t.Errorf("Did not receive correct null flag (%v) (%v)", value, expectedOk)
		}
		if expectedValue!= value {
			t.Errorf("Did not receive correct value (%v) (%v)", value, expectedValue)
		}
	}
}

func checkFloat64(expectedValue float64, expectedOk bool) func (t *testing.T, stmt *Statement) {
	return func (t *testing.T, stmt *Statement) {
		value, ok, err := stmt.GetDataFloat64(1)
		if err != nil {
			t.Errorf("Could not get data (%s)", err)
		}
		if expectedOk!=ok {
			t.Errorf("Did not receive correct null flag (%v) (%v)", value, expectedOk)
		}
		if expectedValue!= value {
			t.Errorf("Did not receive correct value (%v) (%v)", value, expectedValue)
		}
	}
}

func checkString(expectedValue string, expectedOk bool) func (t *testing.T, stmt *Statement) {
	return func (t *testing.T, stmt *Statement) {
		value, ok, err := stmt.GetDataString(1)
		if err != nil {
			t.Errorf("Could not get data (%s)", err)
		}	
		if expectedOk!=ok {
			t.Errorf("Did not receive correct null flag (%v) (%v)", value, expectedOk)
		}
		if expectedValue!= value {
			t.Errorf("Did not receive correct value (%v) (%v)", value, expectedValue)
		}
	}
}

func TestGetData(t *testing.T) {
	tests := []getDataTest {
		{ "SELECT 15;", checkInt32(15,true) },
		{ "SELECT -10;", checkInt32(-10,true) },
		{ "SELECT NULL;", checkInt32(0,false) },
		{ "SELECT 15;", checkInt64(15,true) },
		{ "SELECT -10;", checkInt64(-10,true) },
		{ "SELECT NULL;", checkInt64(0,false) },
		{ "SELECT 15;", checkFloat64(15,true) },
		{ "SELECT -10;", checkFloat64(-10,true) },
		{ "SELECT 15.2;", checkFloat64(15.2,true) },
		{ "SELECT -10.2;", checkFloat64(-10.2,true) },
		{ "SELECT NULL;", checkFloat64(0,false) },
		{ "SELECT 15;", checkString("15",true) },
		{ "SELECT -10;", checkString("-10",true) },
		{ "SELECT 'Hello, world!';", checkString("Hello, world!",true) },
		{ "SELECT NULL;", checkString("",false) },
	}
	
	env, err := NewEnvironment()
	if err != nil {
		t.Fatalf("Could not allocate environment (%s)", err)
	}
	defer env.Free()

	dbc, err := env.DriverConnect(DSN)
	if err != nil {
		t.Fatalf("Could not allocate connection (%s)", err)
	}
	defer dbc.Free()

	for _, v := range tests {
		t.Logf("Testing : %s", v.sql)
		stmt, err := dbc.ExecDirect(v.sql)
		if err != nil {
			t.Errorf("Could not execute sql (%s)", err)
		} else {
			defer stmt.Free()

			ok, err := stmt.Fetch()
			if err != nil {
				t.Errorf("Did not get any data (%s)", err)
			}
			if !ok {
				t.Errorf("Did not get any data (no data)")
			} else {
				numdata, err := stmt.NumResultCols()
				if numdata != 1 || err != nil {
					t.Errorf("Did not get the correct number of columns")
				}
				
				v.pred(t,stmt)
			}
		}
	}
}
