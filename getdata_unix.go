// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build !windows

package odbc3

import (
	"unsafe"
)

func (s *Statement) getDataStringW(index int) (string, bool, error) {
	parts := make([]uint16, 0)

	for {
		chunkSize := int32(2048)
		chunk := make([]uint16, chunkSize)
		var fl int32

		ret := getData(s, uint16(index), int16(C_WCHAR), unsafe.Pointer(&chunk[0]), chunkSize*2, &fl)
		if ret == 100 /*C.SQL_NO_DATA*/ {
			break
		}
		if !success(ret) {
			return "", false, s.getError()
		}
		if fl < 0 {
			return "", false, nil
		}
		parts = append(parts, chunk[0:fl]...)
	}

	return utf16ToString(parts), true, nil
}
