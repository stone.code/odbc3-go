// Copyright 2012 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package odbc3

import (
	"unicode/utf16"
	"unsafe"
)

// A Statement represents a database query, either in progress or in preparation.
type Statement struct {
	private int
}

// Cancel cancels processing of the statement
//
// This function wraps the C API function SQLCancel.
func (s *Statement) Cancel() error {
	ret := cancel(s)
	if !success(ret) {
		return s.getError()
	}
	return s.getError()
}

// ColAttributeString returns information about attributes of a column, but only when that attribute is a number.
//
// This function wraps the C API function SQLColAttribute.
func (s *Statement) ColAttributeNumeric(index int, field FieldIdentifier) (uint32, error) {
	var value uint32

	ret := colAttribute(s, uint16(index), field, nil, 0, nil, &value)
	if !success(ret) {
		return 0, s.getError()
	}
	return value, nil
}

// ColAttributeString returns information about attributes of a column, but only when that attribute is a string.
//
// This function wraps the C API function SQLColAttribute.
func (s *Statement) ColAttributeString(index int, field FieldIdentifier) (string, error) {
	var stringLength int16
	ret := colAttribute(s, uint16(index), field, nil, 0, &stringLength, nil)
	if !success(ret) {
		return "", s.getError()
	}

	buffer := make([]uint16, int(stringLength/2)+1)
	ret = colAttribute(s, uint16(index), field, &buffer[0], int16(len(buffer))*2, &stringLength, nil)
	if !success(ret) {
		return "", s.getError()
	}

	return string(utf16.Decode(buffer[0 : int(stringLength)/2])), nil
}

// Execute runs the already prepared SQL statement.
// Values from bound parameters are used as to complete the statement.
//
// This function wraps the C API function SQLExecute.
func (s *Statement) Execute() error {
	ret := execute(s)
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// ExecDirection prepares and then immediately executes the SQL statement.
//
// This function wraps the C API function SQLExecDirect.
func (s *Statement) ExecDirect(sql string) error {
	tmp := utf16.Encode([]rune(sql))
	ret := execDirect(s, &tmp[0], int32(len(tmp)))
	if ret == 100 /*C.SQL_NO_DATA*/ {
		return nil
	}
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// Fetch obtains the next row of the dataset.
//
// This function wraps the C API function SQLFetch.
func (s *Statement) Fetch() (ok bool, err error) {
	ret := fetch(s)
	if ret == 100 /*C.SQL_NO_DATA*/ {
		return false, nil
	}
	if !success(ret) {
		return false, s.getError()
	}
	return true, nil
}

// FetchScroll obtains the requested row of the dataset.
//
// This function wraps the C API function SQLFetchScroll.
func (s *Statement) FetchScroll(orientation FetchDirection, offset int32) (ok bool, err error) {
	ret := fetchScroll(s, orientation, offset)
	if ret == 100 /*C.SQL_NO_DATA*/ {
		return false, nil
	}
	if !success(ret) {
		return false, s.getError()
	}
	return true, nil
}

// Free releases all resources associated with the statement.
//
// This function wraps the C API function SQLFreeHandle.
func (s *Statement) Free() error {
	ret := freeHandle(HANDLE_STMT, unsafe.Pointer(s))
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// NumParams returns the number of parameters required for the SQL statement.
//
// This function wraps the C API function SQLNumParams.
func (s *Statement) NumParams() (int, error) {
	value := int16(0)
	ret := numParams(s, &value)
	if !success(ret) {
		return 0, s.getError()
	}
	return int(value), nil
}

// NumResultCols returns the number of columns resulting from the SQL statement.
//
// This function wraps the C API function SQLNumResultCols.
func (s *Statement) NumResultCols() (int, error) {
	value := int16(0)
	ret := numResultCols(s, &value)
	if !success(ret) {
		return 0, s.getError()
	}
	return int(value), nil
}

// Prepare parses and prepares and SQL statement for execution.
//
// This function wraps the C API function SQLPrepare.
func (s *Statement) Prepare(sql string) error {
	usql := utf16.Encode([]rune(sql))
	ret := prepare(s, &usql[0], int32(len(usql)))
	if !success(ret) {
		return s.getError()
	}
	return nil
}

// RowCount returns the number of rows modified by the last SQL statement executed.
//
// This function wraps the C API function SQLRowCount.
func (s *Statement) RowCount() (int32, error) {
	var rowcount int32
	ret := rowCount(s, &rowcount)
	if !success(ret) {
		return 0, s.getError()
	}
	return rowcount, nil
}
